README
======

Génération d'une CA
--------------------

     $ certtool --generate-privkey --outfile ca.key
     $ certtool --generate-self-signed --load-privkey ca.key --outfile ca.crt

  * La plupart des champs peuvent rester vides.
  * Common name: CA
  * The certificate will expire in (days): 255
  * Does the certificate belong to an authority? (y/N): y
  * Will the certificate be used to sign other certificates? (y/N): y

Pour afficher et vérifier notre CA :

     $ certtool --infile ca.crt --certificate-info    # ou option -i

Test du Proxy SSL en local
--------------------------

     $ sudo groupadd noredirect
     $ sudo iptables -t nat -F

Pour la version de démo simplifiée :

     $ sudo iptables -t nat -A OUTPUT -m owner ! --gid-owner noredirect -d www.labri.fr -p tcp --dport 443 -j REDIRECT --to-port 4444
     $ sudo sg noredirect './sslproxy-lite.py www.labri.fr 443 4444'
     $ wget -4 -nd --ca-certificate=ca.crt https://www.labri.fr/perso/esnard/hello.html

Pour la version complète (cf. script start.sh) :

     $ sudo iptables -t nat -A OUTPUT -m owner ! --gid-owner noredirect  -p tcp  -m multiport --dports 80,443 -j REDIRECT --to-port 4444
     $ sudo sg noredirect './sslproxy.py 4444'
     $ wget -4 -p --ca-certificate=ca.crt https://www.python.org

Test du proxy SSL dans un LAN
------------------------------

On considère le LAN suivant (192.168.0.0/24) :

~~~
   V ---- [LAN] ----- GW -----> Internet
            |
           PROXY
~~~

avec :
  * V, la victime (client HTTPS) ;
  * PROXY, la machine du LAN qui héberge notre proxy SSL ;
  * GW, la gateway vers Internet !

On commence sur le PROXY :

     $ sudo iptables -A OUTPUT -p icmp --icmp-type redirect -j DROP
     $ sudo iptables -t nat -A PREROUTING -p tcp --dport 443 -j REDIRECT --to-port 4444
     $ sudo bash -c 'echo 1 > /proc/sys/net/ipv4/ip_forward'
     $ ./sslproxy.py 4444

Puis on continue sur V :

     $ route del default
     $ route add default gw @PROXY
     $ wget -4 -p --ca-certificate=ca.crt https://www.python.org

Documentation
-------------

  * http://www.bortzmeyer.org/https-interception.html
  * http://roberts.bplaced.net/index.php/linux-guides/centos-6-guides/proxy-server/squid-transparent-proxy-http-https
  * http://wiki.squid-cache.org/Features/HTTPS
  * https://mitmproxy.org/doc/howmitmproxy.html
  * http://docs.mitmproxy.org/en/latest/transparent.html
  * https://blog.heckel.xyz/2013/07/01/how-to-use-mitmproxy-to-read-and-modify-https-traffic-of-your-phone/

About X509 Certifcates:

  * SNI : https://fr.wikipedia.org/wiki/Server_Name_Indication
  * SAN : https://en.wikipedia.org/wiki/Subject_Alternative_Name

About TCP Proxy:

  * http://voorloopnul.com/blog/a-python-proxy-in-less-than-100-lines-of-code/
