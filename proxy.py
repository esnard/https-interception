#!/usr/bin/python3
import socket
import ssl
import sys
import OpenSSL
import time
import datetime
import os

BUFSIZE = 4096

FAKE_WWW_CERT = "fake.crt"
FAKE_WWW_KEY = "fake.key"

######### MISC #########

def log_message(format, *args):
    sys.stderr.write("[%s] %s\n" % (datetime.datetime.now().strftime("%y-%m-%d %H:%M:%S"), format%args))

######### Main Serve Routine  #########

# handle one connection
def serve(clientconn, clientaddr, serveraddr):
    (serverhost, serverport) = serveraddr       # the actual server
    (clienthost, clientport) = clientaddr       # the actual client

    # wrap client connection with SSL context
    # clientcontext = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    # clientcontext.load_cert_chain(certfile=FAKE_WWW_CERT, keyfile=FAKE_WWW_KEY)
    # clientconnssl = clientcontext.wrap_socket(clientconn, server_side=True)

    serverconn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # connect to server with SSL context
    # servercontext = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
    # serverconnssl = servercontext.wrap_socket(serverconn, server_side=False, server_hostname=serverhost)

    try:
        serverconn.connect(serveraddr)
    except ssl.SSLError as e:
        log_message("%s", e)
        clientconn.close()
        return

    # start talking in SSL/TLS
    sys.stdout.write("~~~~~ REQUEST ~~~~~\n")
    buffer = clientconn.recv(BUFSIZE)
    sys.stdout.write("%s" % buffer.decode())
    serverconn.sendall(buffer)
    sys.stdout.write("~~~~~ ANSWER ~~~~~\n")
    buffer = serverconn.recv(BUFSIZE)
    sys.stdout.write("%s" % buffer.decode())
    clientconn.sendall(buffer)
    buffer = serverconn.recv(BUFSIZE)
    sys.stdout.write("%s" % buffer.decode())
    clientconn.sendall(buffer)

    # close sockets
    serverconn.close()
    clientconn.close()
    log_message('all connections closed')

######### Main Loop #########

def main():

    if len(sys.argv) != 4:
        sys.stderr.write("usage: proxy <server hostname> <server port> <proxy port>\n")
        sys.exit(1)

    SERVERHOST = sys.argv[1]
    SERVERPORT = int(sys.argv[2])
    PROXYHOST = ''
    PROXYPORT = int(sys.argv[3])

    proxyaddr = (PROXYHOST, PROXYPORT)
    serveraddr = (SERVERHOST, SERVERPORT) # the target server

    # create proxy socket listening on PROXYPORT
    proxysocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    proxysocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    proxysocket.bind(proxyaddr)
    proxysocket.listen()

    # main loop
    log_message('proxy listening on port: %d', PROXYPORT)
    log_message('target server: %s', serveraddr)

    while True:
        try:
            log_message("waiting new connection...")
            clientconn, clientaddr = proxysocket.accept()
            serve(clientconn, clientaddr, serveraddr)
        except KeyboardInterrupt:
            log_message("shutting down!")
            proxysocket.close()
            break

######### Main Program  #########

main()
