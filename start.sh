#!/bin/bash
rm -f log
sudo groupadd -f noredirect
sudo iptables -t nat -F
sudo iptables -t nat -A OUTPUT -m owner ! --gid-owner noredirect  -p tcp  -m multiport --dports 80,443 -j REDIRECT --to-port 4444
sudo sg noredirect './sslproxy.py 4444'

