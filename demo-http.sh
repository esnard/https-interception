#!/bin/bash
sudo groupadd -f noredirect
sudo iptables -t nat -F
sudo iptables -t nat -A OUTPUT -m owner ! --gid-owner noredirect -d www.labri.fr -p tcp  --dport 80 -j REDIRECT --to-port 4444

rm -f hello.html
xterm -hold -e 'sudo sg noredirect "./proxy.py www.labri.fr 80 4444" ' &
sleep 1
xterm -hold -e "wget -4 -nd --ca-certificate=ca.crt http://www.labri.fr/perso/esnard/hello.html" &
