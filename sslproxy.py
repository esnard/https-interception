#!/usr/bin/python3
import socket
import threading
import ssl
import sys
import pprint
import OpenSSL
import time
import datetime
import os
import struct
import select

LOG = "log"
CACERT_FILE = "ca.crt"
CAKEY_FILE = "ca.key"
PROXYHOST = ''
PROXYPORT = int(sys.argv[1])
BUFSIZE = 4096
TIMEOUT = 2
DEBUG = False
MULTITHREAD = True
DELAY = 0.001

######### MISC #########

lock = threading.Lock()  # global variable

def threadid():
    # return threading.get_ident() % 100
    return threading.current_thread().name
    # return threading.current_thread().name + "/" + str(threading.active_count())

def log_message(format, *args):
    sys.stderr.write("[%s] %s -- %s\n" % (datetime.datetime.now().strftime("%y-%m-%d %H:%M:%S"), threadid(), format%args))

def log_file(path, data): # input data as bytes
    lock.acquire()
    f = open(path, "ab")
    f.write(data)
    f.close()
    lock.release()

def log_file_utf8(path, data): # input data as bytes
    # try to decode data with the right encoding
    encodings = ['utf8', 'latin1', 'ascii', 'utf16', 'fail']
    str = 'error: encoding not found!\n'
    for e in encodings:
        try:
            str = data.decode(e)
        except UnicodeDecodeError:
            pass
        except LookupError:
            print('Error: encoding not found!') # fail encoding is not valid
            return
        else:
            break
    # log file in utf8
    lock.acquire()
    f = open(path, "ab")
    f.write(str.encode('utf8'))
    f.close()
    lock.release()

# str = bytes.decode()
# bytes = str.encode()

######### HTTP Analysis #########

# parse http message 
def parse_http(msg): # msg as bytes
    firstline = data = b'' ; header = {}
    if(len(msg) == 0): return (firstline, header, data)
    try:
        headerlines, data = msg.split(b'\r\n\r\n',1)
        headerlines += b'\r\n'
        firstline, nextlines = headerlines.split(b'\r\n',1)
        fields = nextlines.split(b'\r\n')
    except ValueError:
        return (firstline, header, data)
    for field in fields:
        try:
            key, value = field.split(b':', 1) # split each line by http field name and value
            header[key.lower()] = value.lower().strip()  # strip() remove extra spaces
        except ValueError:
            continue
    return (firstline, header, data) # return as bytes

# parse header
def parse_header(msg): # msg as bytes
    (firstline, header, _) = parse_http(msg)
    host = b'unknown'
    try:
        host = header[b'host']
    except: pass
    contenttype = b'unknown'
    try:
        contenttype = header[b'content-type']
    except: pass    
    return (firstline.decode(), host.decode(), contenttype.decode()) # return as string

######### Certificate Tools #########

# cert input are assuming to be x509

# save x509 cert file (PEM format)
def save_cert(cert, path):
    lock.acquire()
    certfile = open(path, "wb")
    certfile.write(OpenSSL.crypto.dump_certificate(OpenSSL.crypto.FILETYPE_PEM, cert))
    certfile.close()
    lock.release()

# save private key file (PEM format)
def save_key(key, path):
    lock.acquire()
    keyfile = open(path, "wb")
    keyfile.write(OpenSSL.crypto.dump_privatekey(OpenSSL.crypto.FILETYPE_PEM, key))
    keyfile.close()
    lock.release()

def load_cert(path):
    lock.acquire()
    certfile = open(path, 'rt')
    cert = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, certfile.read())
    certfile.close()
    lock.release()
    return cert

def load_key(path):
    lock.acquire()
    keyfile = open(path, 'rt')
    key = OpenSSL.crypto.load_privatekey(OpenSSL.crypto.FILETYPE_PEM, keyfile.read())
    keyfile.close()
    lock.release()
    return key

######### NAT #########

# Retrieve the original address *after* NAT'ing a connection
# => https://mail.python.org/pipermail/python-list/2010-May/576780.html
# => http://stackoverflow.com/questions/30571630/python-iptables-original-destination-ip
def get_original_dst(conn):
    SO_ORIGINAL_DST = 80 # hack value because socket.SO_ORIGINAL_DST does not currently exist in Python3!
    dst = conn.getsockopt(socket.SOL_IP,SO_ORIGINAL_DST, 16)
    # (proto, port, a, b, c, d) = struct.unpack('!HHBBBB', dst[:8])
    (port, ip) = struct.unpack("!2xH4s8x", dst)
    return (socket.inet_ntoa(ip), port)

######### Generate Fake Certificate #########

def generate_fake_cert(cert, cacert, cakey):

    # get CN & SAN from x509 cert
    cn = cert.get_subject().CN
    ext = None
    for idx in range(cert.get_extension_count()):
        ext = cert.get_extension(idx)
        if(ext.get_short_name() == b'subjectAltName'): break
    # if DEBUG: print("  * SAN = ", ext.get_data()) # in raw format (one should decode it...)

    # create a key pair
    fakekey = OpenSSL.crypto.PKey()
    fakekey.generate_key(OpenSSL.crypto.TYPE_RSA, 1024)

    # create a new x509 cert
    fakecert = OpenSSL.crypto.X509()  # x509 format
    fakecert.get_subject().CN = cn
    fakecert.set_serial_number(int(time.time()))
    fakecert.gmtime_adj_notBefore( 0 )             # not valid before today
    fakecert.gmtime_adj_notAfter( 60*60*24*365*5 ) # not valid after 5 years
    fakecert.set_pubkey(fakekey)
    fakecert.set_version(0x2) # set certificate version to X509 v3 (0x2), required for X509 extensions

    # add subjectAltName X509 extension (SAN)
    if ext: fakecert.add_extensions([ext])

    # set issuer and CA signature
    fakecert.set_issuer(cacert.get_subject())
    fakecert.sign(cakey, 'sha1')
    fakecert.sign(cakey, 'sha256')

    return (fakecert, fakekey)

######### Proxy handle SSL  #########

def handle_ssl(clientconn, serverconn, clientaddr, serveraddr):

    sslservercert = ssl.get_server_certificate(serveraddr)
    servercert = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, sslservercert)
    servercn = servercert.get_subject().CN
    if DEBUG: log_message('Get certificate: %s', servercn)

    fakecert, fakekey = generate_fake_cert(servercert, cacert, cakey)
    servercertpath = "/tmp/" + servercn + '.crt.original'
    fakecertpath = "/tmp/" + servercn + '.crt'
    fakekeypath = "/tmp/" + servercn + '.key'
    save_cert(servercert, servercertpath)
    save_cert(fakecert, fakecertpath)
    save_key(fakekey, fakekeypath)
    if DEBUG: log_message('Generate fake certificate: %s', servercn)

    # wrap client connection with SSL context
    clientcontext = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    clientcontext.load_cert_chain(certfile=fakecertpath, keyfile=fakekeypath)
    clientconnssl = clientcontext.wrap_socket(clientconn, server_side=True)

    # wrap server connection with SSL context
    servercontext = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
    servercontext.check_hostname = False
    serverconnssl = servercontext.wrap_socket(serverconn, server_side=False)
    # no SNI used, because it requires to parse HTTP request header (host field) on the fly!
    # serverconnssl = servercontext.wrap_socket(serversocket, server_side=False, server_hostname=servername)

    return (clientconnssl, serverconnssl)

######### Handle Request & Response  #########

def handle_message(socket_from, socket_to, msg_type):
    # if DEBUG: log_message("receiving %s", msg_type)
    msg = b''
    chunkidx = 0
    while True:
        chunk = socket_from.recv(BUFSIZE)
        msg += chunk
        if DEBUG: log_message("%s chunk #%d: %d bytes", msg_type, chunkidx, len(chunk))
        socket_to.sendall(chunk)
        if len(chunk) < BUFSIZE: break  # is that condition enough to detect msg end? clearly not!
        chunkidx += 1
    return msg

######### Main Serve Routine  #########

# handle one connection, but possibly multiple requests & responses
def serve(clientconn, clientaddr):
    serveraddr = get_original_dst(clientconn)   # FileNotFoundError ?
    (serverhost, serverport) = serveraddr
    (clienthost, clientport) = clientaddr

    # handle (secure) connections
    serverconn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    protocol = "http"
    if serverport == 443:
        protocol = "https"
        (clientconn, serverconn) = handle_ssl(clientconn, serverconn, clientaddr, serveraddr)
    try:
        serverconn.connect(serveraddr)
    except ssl.SSLError as e:
        log_message("%s", e)
        clientconn.close()
        return

    # start talking...
    channel = {clientconn:serverconn, serverconn:clientconn}
    listr = [clientconn,serverconn]
    reqidx = rspidx = pieceidx = 0
    request = response = b''
    currentconn = 0
    cont = True
    firstline = contenttype = host = ''
    
    while cont:
        time.sleep(DELAY)

        r, _, _ = select.select(listr, [], [], TIMEOUT)
        assert(len(r) <= 1) # I cannot imagine this scenario on proxy!
        if(len(r) == 0):
            if DEBUG: log_message("select timeout!")
            break
        # if DEBUG: log_message("select wakes up with %d events...", len(r))
        
        for s in r:
            # new incoming request (end of previous response)
            if s == clientconn and currentconn != clientconn:
                # response end
                if currentconn != 0:
                    (firstline, _, contenttype) = parse_header(response)
                    log_message('%s://%s <= "%s" (%d bytes, %s)', protocol, host, firstline, len(response), contenttype)
                    if "text" in contenttype: log_file_utf8(LOG, request + response) # logging !
                # new request
                currentconn = clientconn
                request = response =  b''
                reqidx += 1
                pieceidx = 0
                if DEBUG: log_message('recv new request #%d', reqidx)
            # new incoming response (end of previous request)
            elif s == serverconn and currentconn != serverconn:
                # request end
                (firstline, host, _) = parse_header(request)
                log_message('%s://%s => "%s" (%d bytes)', protocol, host, firstline[:80], len(request))
                # new response
                currentconn = serverconn
                rspidx += 1
                pieceidx = 0
                if DEBUG: log_message('recv new response #%d', rspidx)

            # request piece
            if s == clientconn:
                msgtype = 'request #%d piece #%d' % (reqidx, pieceidx)
                piece = handle_message(s, channel[s], msgtype)
                request += piece
                pieceidx += 1
                if(len(piece) == 0): cont = False ; continue
            # response piece
            else:
                msgtype = 'response #%d piece #%d' % (rspidx, pieceidx)
                piece = handle_message(s, channel[s], msgtype)
                response += piece
                pieceidx += 1
                if(len(piece) == 0): cont = False ; continue

    # handle the last response (if no empty request was sent to close the keep-alive connection...)
    if(len(request) > 0):
        (firstline, _, contenttype) = parse_header(response)
        log_message('%s://%s <= "%s" (%d bytes, %s)', protocol, host, firstline, len(response), contenttype)
        if "text" in contenttype: log_file_utf8(LOG, request + response) # logging !
                
    # close sockets
    serverconn.close()
    clientconn.close()
    if DEBUG: log_message('connection close')

######### Main Program  #########

# load CA cert & CA key (x509)
cacert = load_cert(CACERT_FILE)
cakey = load_key(CAKEY_FILE)

# create proxy socket listening on PROXYPORT
proxysocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
proxysocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
proxysocket.bind((PROXYHOST, PROXYPORT))
proxysocket.listen()
# proxyaddr = proxysocket.getsockname()

# main loop
log_message('listening on port: %d', PROXYPORT)

while True:
    try:
        if DEBUG: log_message("waiting new connection...")
        clientconn, clientaddr = proxysocket.accept()
        if MULTITHREAD:
            threading.Thread(target=serve,args=(clientconn, clientaddr)).start()
        else:
            serve(clientconn, clientaddr)
    except KeyboardInterrupt:
        log_message("shutting down!")
        proxysocket.close()
        break
